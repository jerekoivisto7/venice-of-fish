using UnityEngine;

public class ResetButtonScript : MonoBehaviour
{
    // References to other scripts and components needed for reset functionality
    public PlayerMovement playerMovement; // Reference to the PlayerMovement script
    public AIMovement aiMovement; // Reference to the AIMovement script
    public GameManager gameManager; // Reference to the GameManager script
    public TileColorManager tileColorManager; // Reference to the TileColorManager script

    // Called when the reset button is clicked
    public void OnResetButtonClicked()
    {
        Time.timeScale = 1f; // Reset time scale to normal (in case it was paused during the game)

        // Reset player and AI positions to their initial positions
        playerMovement.ResetPlayerPosition();
        aiMovement.ResetAIPosition();

        // Nullify points for both the player and AI
        playerMovement.ResetPoints();
        aiMovement.ResetPoints();

        // Reset fog on the tiles to its initial state
        tileColorManager.StartCoroutine(tileColorManager.ResetFog());

        // Reset game manager variables and restart the game flow
        gameManager.ResetVariables();
    }
}
