using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileColorManager : MonoBehaviour
{
    public SpriteRenderer[] tileRenderers; // Array of SpriteRenderers representing the tiles
    public Color[] tierColors; // Array of colors representing different tiers of tiles
    private int currentColorIndex = 0; // Index to track the current color set for the tiles
    private int previousColorIndex = -1; // Index to track the previous color set for the tiles
    private List<int> visitedTiles = new List<int>(); // List to keep track of visited tiles

    public PlayerMovement playerMovement; // Reference to the PlayerMovement script
    private BoxCollider2D playerCollider; // Rectangle collider attached to the player

    private void Start()
    {
        playerMovement = FindObjectOfType<PlayerMovement>(); // Find and assign the reference to PlayerMovement component in the scene

        if (playerMovement == null)
        {
            Debug.LogError("PlayerMovement component not found in the scene.");
            return;
        }

        playerCollider = playerMovement.GetComponent<BoxCollider2D>(); // Get the BoxCollider2D component attached to the player

        SetTileColors(); // Set the initial colors for the tiles
    }

    // Set colors for the tiles based on the player's position and visited tiles
    public void SetTileColors()
    {
        int currentTileIndex = playerMovement.GetCurrentTileIndex(); // Get the index of the tile the player is currently on

        // Calculate the bounds of the player's rectangle collider
        Bounds playerBounds = playerCollider.bounds;

        foreach (SpriteRenderer tileRenderer in tileRenderers)
        {
            int tier = tileRenderer.transform.GetSiblingIndex() / playerMovement.tilesPerTier; // Calculate the tier of the tile based on its sibling index
            Color tileColor = Color.grey; // Start with the default color as grey

            // Check if the tile is the one currently clicked by the player or is in the visited tiles list
            if (visitedTiles.Contains(tileRenderer.transform.GetSiblingIndex()))
            {
                Color originalColor = GetColorForTier(tier); // Get the original color for the tier
                tileColor = new Color(originalColor.r, originalColor.g, originalColor.b, 1.0f); // Create a new color with alpha = 1.0f (fully opaque)
            }
            else
            {
                // Check if the tile's collider is colliding with the player's collider
                Collider2D tileCollider = tileRenderer.GetComponent<Collider2D>();

                if (tileCollider != null && tileCollider.bounds.Intersects(playerBounds))
                {
                    tileColor = GetColorForTier(tier); // Get the color for the tier if the player is on this tile
                }
            }

            tileRenderer.color = tileColor; // Set the color for the tile
        }
    }

    // Get the color for a given tier based on the current color index
    public Color GetColorForTier(int tier)
    {
        int colorIndex = (tier + currentColorIndex) % tierColors.Length; // Calculate the index of the color for the given tier
        Color color = tierColors[colorIndex]; // Get the color from the tierColors array
        color.a = 1.0f; // Set the alpha value to 1.0f (fully opaque)
        return color; // Return the resulting color
    }

    // Change the colors of the tiles based on the current color index and random increment
    public void ChangeTileColors()
    {
        int randomIncrement = Random.Range(1, tierColors.Length); // Generate a random increment value within the range of tierColors array length
        currentColorIndex = (currentColorIndex + randomIncrement) % tierColors.Length; // Update the current color index with the random increment

        // Check if the color index has changed to prevent redundant color updates
        if (currentColorIndex != previousColorIndex)
        {
            SetTileColors(); // Set the new colors for the tiles based on the updated color index
            previousColorIndex = currentColorIndex; // Update the previous color index for comparison in the next frame
        }
    }

    // Add the given tile index to the list of visited tiles
    public void AddVisitedTile(int tileIndex)
    {
        if (!visitedTiles.Contains(tileIndex))
        {
            visitedTiles.Add(tileIndex);
        }
    }

    // Coroutine to reset the fog by clearing the list of visited tiles and setting tile colors again after a delay
    public IEnumerator ResetFog()
    {
        yield return new WaitForSeconds(0.5f); // Wait for 0.5 seconds before resetting fog
        visitedTiles.Clear(); // Clear the list of visited tiles
        SetTileColors(); // Set the tile colors again to reset the fog
    }
}
