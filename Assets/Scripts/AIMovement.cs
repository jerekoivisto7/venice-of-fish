using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    // Public variables accessible in the Inspector
    public Transform[] tiles;
    public float moveDelay = 1f;
    public Vector2 initialPosition;

    // Private variables
    private Transform aiTransform;
    private Transform targetTileTransform;
    public bool isMoving = false;
    private bool pointsDeducted = false;
    public bool sabotageClicked = false;
    public int aiPoints = 0;
    public TMP_Text aiScoreText;
    public int tilesPerTier = 4;
    private List<int> visitedTiles = new List<int>();

    // References to other scripts and components
    private SpriteRenderer aiSpriteRenderer;
    public GameManager gameManager;
    public PlayerMovement playerMovement;

    // Initialization at the start of the game
    private void Start()
    {
        // Get the SpriteRenderer component for the AI GameObject
        aiSpriteRenderer = GetComponent<SpriteRenderer>();
        AIInvisible(); // Make the AI invisible
        aiTransform = transform;
        aiTransform.position = initialPosition;
        aiPoints = 0;
        UpdateAIScoreText(); // Update the AI score text on the UI
    }

    // The AI's main decision-making method
    public void MakeMove()
    {
        if (!isMoving)
        {
            // Select a random adjacent tile to move to
            targetTileTransform = GetRandomAdjacentTile(aiTransform.position);

            if (targetTileTransform != null)
            {
                if (!IsWhiteTile(targetTileTransform) && targetTileTransform != aiTransform)
                {
                    // Start moving towards the target tile
                    isMoving = true;
                    StartCoroutine(MoveToTarget());
                }
                else
                {
                    // Handle case when AI selects a white tile, try to select a different tile
                    Debug.Log("AI cannot move to a white tile.");
                    MakeMove();
                }
            }
        }

        // Check if the AI has reached its target tile and deduct points if it's a white tile
        if (!isMoving && !pointsDeducted)
        {
            if (targetTileTransform != null)
            {
                Color tileColor = targetTileTransform.GetComponent<SpriteRenderer>().color;

                if (tileColor == Color.white)
                {
                    LosePoints(2);
                    UpdateAIScoreText();
                    pointsDeducted = true;
                }
            }

            // If points are already deducted, skip the next section
            if (pointsDeducted)
            {
                return;
            }
        }

        // Check if the player is nearby the AI and inside the AI's BoxCollider2D bounds
        Collider2D playerCollider = playerMovement.GetComponent<BoxCollider2D>();
        if (playerCollider != null && playerCollider.bounds.Intersects(aiSpriteRenderer.bounds))
        {
            if (!sabotageClicked)
            {
                // Player is nearby and inside AI's bounds, activate the buttons
                gameManager.helpButton.SetActive(true);
                gameManager.sabotageButton.SetActive(true);

                // Player is near the AI, stop the game loop, AI movement, and player input
                GameManager.Instance.StopGameLoop();
                playerMovement.DisableInput();
            }
        }
    }

    // Coroutine for moving the AI towards its target tile
    private IEnumerator MoveToTarget()
    {
        while (Vector2.Distance(aiTransform.position, targetTileTransform.position) > 0.01f)
        {
            // Move the AI towards the target tile over time
            aiTransform.position = Vector2.MoveTowards(aiTransform.position, targetTileTransform.position, Time.deltaTime * 5f);
            yield return null;
        }

        // Movement complete, stop moving
        isMoving = false;

        // If points are not deducted, check the tile color and handle the points
        if (!pointsDeducted)
        {
            Color tileColor = targetTileTransform.GetComponent<SpriteRenderer>().color;

            if (tileColor == Color.white)
            {
                // AI moved to a white tile, move it to a non-white tile and deduct points
                MoveAIToNonWhiteTile();
                LosePoints(2);
                pointsDeducted = true;
            }
            else
            {
                // AI moved to a non-white tile, add points and mark it as visited if it's on a new tier
                int tier = targetTileTransform.GetSiblingIndex() / tilesPerTier;
                bool hasPreviouslyVisitedSameLevel = visitedTiles.Exists(tileIndex => tileIndex / tilesPerTier + 1 == tier);

                if (!hasPreviouslyVisitedSameLevel)
                {
                    int score = tier - 1;
                    aiPoints += score;
                    UpdateAIScoreText();
                }
            }
        }

        // Reset the pointsDeducted flag for the next move
        pointsDeducted = false;
    }

    // Get a random adjacent tile to the AI's current position
    private Transform GetRandomAdjacentTile(Vector2 currentPosition)
    {
        // Create a list to store adjacent tiles
        List<Transform> adjacentTiles = new List<Transform>();

        // Find adjacent tiles based on position
        foreach (Transform tile in tiles)
        {
            if (tile != aiTransform)
            {
                // Calculate the Manhattan distance between the tile and the AI's current position
                float distance = Mathf.Abs(tile.position.x - currentPosition.x) + Mathf.Abs(tile.position.y - currentPosition.y);

                // Check if the distance is within the range of adjacent tiles
                if (distance <= 6f)
                {
                    adjacentTiles.Add(tile);
                }
            }
        }

        // Return a random adjacent tile if available, otherwise return null
        if (adjacentTiles.Count > 0)
        {
            int randomIndex = Random.Range(0, adjacentTiles.Count);
            return adjacentTiles[randomIndex];
        }
        return null; // No adjacent tile found
    }

    // Move the AI to a non-white tile (used when it lands on a white tile)
    public void MoveAIToNonWhiteTile()
    {
        List<Transform> nonWhiteTiles = new List<Transform>();

        foreach (Transform tile in tiles)
        {
            if (!IsWhiteTile(tile))
            {
                nonWhiteTiles.Add(tile);
            }
        }

        int randomIndex = Random.Range(0, nonWhiteTiles.Count);
        targetTileTransform = nonWhiteTiles[randomIndex];
        aiTransform.position = targetTileTransform.position;
    }

    // Deduct points from the AI and ensure the points value is at least -1
    public void LosePoints(int points)
    {
        aiPoints -= points;
        aiPoints = Mathf.Max(aiPoints, -1);
        UpdateAIScoreText(); // Update the AI score text on the UI
    }

    // Check if a tile is white based on its color
    private bool IsWhiteTile(Transform tile)
    {
        Color tileColor = tile.GetComponent<SpriteRenderer>().color;
        return tileColor == Color.white;
    }

    // Update the AI score text on the UI
    private void UpdateAIScoreText()
    {
        aiScoreText.text = "AI Score: " + aiPoints;
    }

    // Add the index of a visited tile to the list of visitedTiles
    public void AddVisitedTile(int tileIndex)
    {
        if (!visitedTiles.Contains(tileIndex))
        {
            visitedTiles.Add(tileIndex);
        }
    }

    // Reset the AI's position to the initial position
    public void ResetAIPosition()
    {
        aiTransform.position = initialPosition;
    }

    // Reset the AI's points to 0
    public void ResetPoints()
    {
        aiPoints = 0;
        UpdateAIScoreText(); // Update the AI score text on the UI
    }

    // Make the AI visible by setting its alpha value to 1
    public Color GetColorForAI()
    {
        Color aiColor = aiSpriteRenderer.color;
        aiColor.a = 1f;
        aiSpriteRenderer.color = aiColor;
        return aiColor;
    }

    // Make the AI invisible by setting its alpha value to 0
    public Color AIInvisible()
    {
        Color aiColor = aiSpriteRenderer.color;
        aiColor.a = 0f;
        aiSpriteRenderer.color = aiColor;
        return aiColor;
    }

    // Move the AI backward by a certain number of tiles
    public void MoveAIBackward(int tilesToMove)
    {
        // Get the current tile index of the AI
        int currentIndex = targetTileTransform.GetSiblingIndex();

        // Calculate the new tile index to move backward
        int newIndex = Mathf.Max(0, currentIndex - tilesToMove);

        // Get the tile transform for the new index
        targetTileTransform = tiles[newIndex];

        // Move the AI to the new tile
        StartCoroutine(MoveToTarget());
    }
}
