using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    // References to other scripts and UI elements
    public PlayerMovement playerMovement;
    public AIMovement aiMovement;
    public TileColorManager tileColorManager;
    public TMP_Text turnText;
    public TMP_Text waterLevelText;
    public TMP_Text gameOverText;
    public GameObject resetBackground;
    public GameObject gameOverButton;
    public GameObject helpButton;
    public GameObject sabotageButton;
    public AudioSource playerSiren;
    public AudioSource aiSiren;

    // Game state variables
    private bool isPlayerTurn = true;       // Flag to indicate if it's the player's turn
    public bool hasAIPlayed = false;        // Flag to check if AI has played during its turn
    private bool colorsChanged = false;     // Flag to check if tile colors have been changed during player's turn
    private int waterLevel = 1;             // Current water level of the game

    // Singleton pattern to ensure there's only one GameManager instance
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    // Initialize game state and start the main game flow coroutine
    private void Start()
    {
        isPlayerTurn = true;
        hasAIPlayed = false;
        waterLevel = 1;
        tileColorManager.SetTileColors();
        tileColorManager.ChangeTileColors();
        UpdateWaterLevelText();
        StartCoroutine(GameFlow());
    }

    // The main game flow coroutine that runs in an endless loop
    private IEnumerator GameFlow()
    {
        while (true)
        {
            // Change water levels and colors only if it's the player's turn and colors haven't been changed yet
            if (isPlayerTurn && !colorsChanged)
            {
                playerMovement.DisableInput();
                turnText.text = "Changing water level";
                ChangeWaterLevel();
                tileColorManager.ChangeTileColors();
                colorsChanged = true;
                yield return new WaitForSeconds(3.0f);
            }

            if (isPlayerTurn)
            {
                // Player's turn starts
                turnText.text = "Player's turn";
                playerMovement.EnableInput();
                playerSiren.Play();
                yield break; // Exit the coroutine and wait for player input
            }
            else
            {
                // AI's turn starts
                if (!hasAIPlayed)
                {
                    turnText.text = "AI's turn";
                    aiMovement.MakeMove();
                    hasAIPlayed = true;
                    yield return new WaitUntil(() => !aiMovement.isMoving);
                    // AI's turn ends
                }
            }

            // Wait for some time before switching turns and checking for game over conditions
            yield return new WaitForSeconds(3.0f);

            // Play AI siren sound
            aiSiren.Play();

            // Switch turns and reset flags
            isPlayerTurn = !isPlayerTurn;
            hasAIPlayed = false;
            colorsChanged = false;

            // Check if the game is over
            if (aiMovement.sabotageClicked == true && (waterLevel == 5 || playerMovement.playerPoints >= 19 || aiMovement.aiPoints >= 19))
            {
                // Disable player input and AI movement
                playerMovement.DisableInput();

                // Display game over message
                turnText.text = "Game Over";
                gameOverText.text = "Game Over!";
                gameOverButton.SetActive(true);
                resetBackground.SetActive(true);

                // Pause the game (Time.timeScale = 0f) to prevent further input
                Time.timeScale = 0f;

                yield break; // Exit the coroutine
            }
        }
    }

    // Randomly change the water level by a certain amount
    private void ChangeWaterLevel()
    {
        int changeAmount = Random.Range(-2, 3); // Randomly change the water level by -2, -1, 0, 1, or 2
        int chance = Random.Range(0, 3); // Randomly determine whether the change is higher or lower

        if (chance < 2) // 2 out of 3 chance to become higher
        {
            waterLevel += changeAmount;
        }
        else
        {
            waterLevel -= changeAmount;
        }

        // Play sound based on the water level change (code for AudioManager is commented out)
        if (changeAmount > 0)
        {
            //AudioManager.Instance.PlaySirenSound(true);
        }
        else if (changeAmount < 0)
        {
            //AudioManager.Instance.PlaySirenSound(false);
        }

        // Clamp the water level between 1 and 5
        waterLevel = Mathf.Clamp(waterLevel, 1, 5);

        UpdateWaterLevelText(); // Update the water level text
    }

    // Update the water level text on the UI
    private void UpdateWaterLevelText()
    {
        waterLevelText.text = "Water Level: " + waterLevel.ToString(); // Update the water level text with the current water level
    }

    // Called when the player ends their turn
    public void EndPlayerTurn()
    {
        // Disable player input
        playerMovement.DisableInput();

        // Set the turn to the AI
        isPlayerTurn = false;

        // Reset the AI played flag
        hasAIPlayed = false;

        // Resume the game flow coroutine to continue the game
        StartCoroutine(GameFlow());
    }

    // Reset game manager variables and start the game flow again
    public void ResetVariables()
    {
        isPlayerTurn = true;
        hasAIPlayed = false;
        waterLevel = 1;
        colorsChanged = false;
        gameOverText.text = ""; // Clear the game over text
        gameOverButton.SetActive(false);
        resetBackground.SetActive(false);
        // Reset sabotage clicked flag in AIMovement script
        SetSabotageClicked(false);
        aiMovement.AIInvisible();
        StartCoroutine(GameFlow()); // Restart the game flow
    }

    // Stop the game flow coroutine (used when resetting the game)
    public void StopGameLoop()
    {
        StopCoroutine(GameFlow());
    }

    // Called when the player clicks the "Help" button
    public void Help()
    {
        // Activate the AI's color and deactivate the sabotage and help buttons
        aiMovement.GetColorForAI();
        sabotageButton.SetActive(false);
        helpButton.SetActive(false);
        playerMovement.EnableInput();
        aiMovement.sabotageClicked = true;
    }

    // Called when the player clicks the "Sabotage" button
    public void Sabotage()
    {
        // Activate sabotage mode, deduct points from both player and AI, move them backward,
        // and deactivate the sabotage and help buttons
        aiMovement.sabotageClicked = true; // Set the sabotageClicked flag in AIMovement script

        aiMovement.LosePoints(4);
        playerMovement.LosePoints(4);
        playerMovement.MovePlayerBackward(4);
        aiMovement.MoveAIBackward(4);
        sabotageButton.SetActive(false);
        helpButton.SetActive(false);
        playerMovement.EnableInput();
    }

    // Setter method to update the sabotageClicked flag in AIMovement script
    public void SetSabotageClicked(bool value)
    {
        aiMovement.sabotageClicked = value;
    }
}
