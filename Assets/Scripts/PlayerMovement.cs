using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Public variables accessible in the Inspector
    public Transform[] tiles;
    public Vector2 initialPosition;
    public float touchThreshold = 0.5f;

    // Private variables
    [SerializeField] public Transform playerTransform;
    private Transform targetTileTransform;
    private bool isMoving;
    private bool isInputEnabled;
    private bool pointsDeducted = false;
    public int playerPoints = 0;
    public TMP_Text scoreText;
    public TileColorManager tileColorManager;
    public int tilesPerTier = 4;

    // Variables for tracking player tile movement
    private int currentTileIndex;
    private List<int> visitedTiles = new List<int>();

    // Initialization at the start of the game
    private void Start()
    {
        playerTransform = transform;
        playerTransform.position = initialPosition;
        isInputEnabled = true;
        playerPoints = 0;
        UpdateScoreText(); // Update the player score text on the UI
    }

    // Update is called once per frame
    private void Update()
    {
        // Check for player input only if input is enabled and the player is not currently moving
        if (isInputEnabled && Input.GetMouseButtonDown(0) && !isMoving)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            // Check if the player clicked on an adjacent tile
            foreach (Transform tile in tiles)
            {
                Vector2 tilePosition = tile.position;

                if (Vector2.Distance(tilePosition, mousePosition) < touchThreshold)
                {
                    if (IsAdjacentToCurrentTile(tile))
                    {
                        if (!IsWhiteTile(tile)) // Check if the tile is not white
                        {
                            targetTileTransform = tile;
                            isMoving = true; // Start moving towards the target tile
                            break;
                        }
                        else
                        {
                            // Handle case when player clicks on a white tile, prevent movement
                            Debug.Log("Cannot move to a white tile.");
                        }
                    }
                }
            }
        }

        // If player is currently moving
        if (isMoving)
        {
            // Move the player towards the target tile over time
            playerTransform.position = Vector2.MoveTowards(playerTransform.position, targetTileTransform.position, Time.deltaTime * 5f);

            // Check if the player has reached the target tile
            if (Vector2.Distance(playerTransform.position, targetTileTransform.position) < 0.01f)
            {
                // Movement complete, stop moving and end the player's turn
                isMoving = false;
                GameManager.Instance.EndPlayerTurn();

                // Add the current tile to the list of visited tiles and update player score based on depth
                AddVisitedTile(currentTileIndex);
                int tier = targetTileTransform.GetSiblingIndex() / tilesPerTier;
                bool hasPreviouslyVisitedSameLevel = visitedTiles.Exists(tileIndex => tileIndex / tilesPerTier + 1 == tier);

                if (!hasPreviouslyVisitedSameLevel)
                {
                    int score = tier - 1;
                    playerPoints += score;
                    UpdateScoreText();
                }
            }
        }

        // Check if the player is currently moving and points have not been deducted
        if (isMoving && !pointsDeducted)
        {
            Color tileColor = targetTileTransform.GetComponent<SpriteRenderer>().color;

            if (tileColor == Color.white)
            {
                // Player moved to a white tile, move to a non-white tile and deduct points
                MovePlayerToNonWhiteTile();
                LosePoints(2);
                pointsDeducted = true;
            }
        }

        // If not moving and points have not been deducted, check the target tile color and handle points
        if (!isMoving && !pointsDeducted)
        {
            if (targetTileTransform != null) // Make sure targetTileTransform is not null
            {
                Color tileColor = targetTileTransform.GetComponent<SpriteRenderer>().color;

                if (tileColor == Color.white)
                {
                    // Player moved to a white tile, deduct points
                    LosePoints(2);
                    UpdateScoreText();
                    pointsDeducted = true;
                }
            }

            // If points are deducted, skip the next section
            if (pointsDeducted)
            {
                return;
            }
        }
    }

    // Enable player input
    public void EnableInput()
    {
        isInputEnabled = true;
    }

    // Disable player input
    public void DisableInput()
    {
        isInputEnabled = false;
    }

    // Check if a tile is adjacent to the player's current tile
    private bool IsAdjacentToCurrentTile(Transform tile)
    {
        float distance = Mathf.Abs(tile.position.x - playerTransform.position.x) + Mathf.Abs(tile.position.y - playerTransform.position.y);
        return distance <= 10f;
    }

    // Check if a tile is white based on its color
    private bool IsWhiteTile(Transform tile)
    {
        Color tileColor = tile.GetComponent<SpriteRenderer>().color;
        return tileColor == Color.white;
    }

    // Move the player to a non-white tile (used when the player lands on a white tile)
    public void MovePlayerToNonWhiteTile()
    {
        List<Transform> nonWhiteTiles = new List<Transform>();

        foreach (Transform tile in tiles)
        {
            if (!IsWhiteTile(tile))
            {
                nonWhiteTiles.Add(tile);
            }
        }

        int randomIndex = Random.Range(0, nonWhiteTiles.Count);
        targetTileTransform = nonWhiteTiles[randomIndex];
        playerTransform.position = targetTileTransform.position;
    }

    // Deduct points from the player and ensure the points value is at least 0
    public void LosePoints(int points)
    {
        playerPoints -= points;
        playerPoints = Mathf.Max(playerPoints, 0);
        UpdateScoreText(); // Update the player score text on the UI
    }

    // Get the index of the current tile that the player is on
    public int GetCurrentTileIndex()
    {
        int closestTileIndex = -1;
        float closestDistance = float.MaxValue;
        Vector2 playerPosition = playerTransform.position;

        for (int i = 0; i < tiles.Length; i++)
        {
            Vector2 tilePosition = tiles[i].position;
            float distance = Vector2.Distance(playerPosition, tilePosition);

            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestTileIndex = i;
            }
        }

        return closestTileIndex;
    }

    // Update the player score text on the UI
    private void UpdateScoreText()
    {
        scoreText.text = "Score: " + playerPoints;
    }

    // Add the index of a visited tile to the list of visitedTiles
    private void AddVisitedTile(int tileIndex)
    {
        if (!visitedTiles.Contains(tileIndex))
        {
            visitedTiles.Add(tileIndex);
        }
    }

    // Reset the player's position to the initial position
    public void ResetPlayerPosition()
    {
        playerTransform.position = initialPosition;
    }

    // Reset the player's points to 0
    public void ResetPoints()
    {
        playerPoints = 0;
        UpdateScoreText(); // Update the player score text on the UI
    }

    // Move the player backward by a certain number of tiles
    public void MovePlayerBackward(int tilesToMove)
    {
        // Get the current tile index of the player
        int currentIndex = targetTileTransform.GetSiblingIndex();

        // Calculate the new tile index to move backward
        int newIndex = Mathf.Max(0, currentIndex - tilesToMove);

        // Get the tile transform for the new index
        targetTileTransform = tiles[newIndex];

        // Move the player to the new tile
        StartCoroutine(MoveToTarget());
    }

    // Coroutine to smoothly move the player to the target tile
    private IEnumerator MoveToTarget()
    {
        while (Vector2.Distance(playerTransform.position, targetTileTransform.position) > 0.01f)
        {
            playerTransform.position = Vector2.MoveTowards(playerTransform.position, targetTileTransform.position, Time.deltaTime * 5f);
            yield return null;
        }
    }
}
