using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGameScript : MonoBehaviour
{
    // This method will be called when the QuitButton is clicked
    public void QuitGame()
    {
        Application.Quit();
    }
}
