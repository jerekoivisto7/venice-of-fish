How to Play


Objective: The objective of the game is to gain as many points as possible while avoiding white tiles. The player must reach non-white tiles to gain points. The game ends when either the water level reaches its maximum value, the player or AI score reaches 19 points, or the AI sabotages the player.

Tiles: The game grid consists of various tiles, each having different colors representing different tiers. White tiles represent flooded areas and must be avoided.

Player Controls: The player can move by clicking on adjacent tiles. When the player clicks on an adjacent tile that is not white, the character will move towards that tile. However, the player cannot move diagonally or select white tiles.

AI Movement: The AI moves automatically during its turn, selecting a random adjacent tile that is not white to move to.

Game Flow: The game flow alternates between the player's turn and the AI's turn. During the player's turn, the player can move by clicking on tiles. After the player's turn, the water level changes, and the tiles' colors may change. Then, it becomes the AI's turn.

Water Level: The water level changes randomly during the game, which may cause the color of the tiles to change. The water level is represented by the text on the top-left corner of the screen.

Points: The player and the AI can gain points by reaching non-white tiles. The AI gets points based on tiers, while the player gains points based on the tier's depth. The points are represented by the "Score" and "AI Score" texts on the screen.

Sabotage: During the game, the player can click the "Sabotage" button to sabotage the AI. It will deduct points from both the player and AI and move them backward.

Game Over: The game ends if the water level reaches its maximum value (5), or the player or AI score reaches 19 points. Additionally, if the AI sabotages the player, the game ends as well.



Scripts Overview
AIMovement: This script manages the movement and actions of the AI character. It determines the AI's movement based on a random adjacent tile and handles the AI's interaction with the player.

GameManager: This script manages the overall game state and flow. It handles the alternating turns between the player and AI, changes the water level, and checks for game-over conditions.

PlayerMovement: This script manages the player's movement, input, and interaction with the tiles. It handles the player's turn and movement to adjacent tiles.

ResetButtonScript: This script resets the game when the "Reset" button is clicked. It resets player and AI positions, points, tile colors, and fog.

TileColorManager: This script manages the colors of the tiles based on the player's position and visited tiles. It sets the initial colors, changes the colors based on the water level, and resets the fog when the player moves.

Known bugs:

- Sometimes score doesn't get added up correctly to AI or player when moving to tiles.
- Sometimes white tiles doesn't make player or AI to lose points if on smaller tiles.
- Sometimes after sabotage game doesn't end when score goes beyond 19.
- Sometimes after sabotage AI's score goes below 0.
- Player cannot move to corner tiles due to movement being based on pivot points.
- Pivot point movement makes player movement not seem like they are on tile at times.
